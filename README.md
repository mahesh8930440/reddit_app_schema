# Reddit app Schema

## Entities:

- User Profile Table
- Post Table
- Comment Table
- Vote Table
- Subreddit Table
- Subscription Table

## Relationships:

- User -> Post (One-to-Many )
- User -> Comment (One-to-Many)
- Post -> Comment (One-to-Many)
- Vote -> User (Many-to-One)
- Vote -> Comment (Many-to-Many)
- Vote -> Post (Many-to-Many)

## User Profile Table:

- id (Primary Key): A unique identifier.
- user_id (Foreign Key): References the id in the User Table.
- display_name: The display name of the user.
- bio: User's bio or description.
- profile_picture_url: URL to the user's profile picture.

## Post Table:

- id (Primary Key): A unique identifier.
- user_id (Foreign Key): References the id in the User Table.
- subreddit_id(Foreign Key):References the id in the subreddit.
- title: The title of the post.
- content: Body of the post.
- created_at: Timestamp indicating when the post was created.

## Comment Table:

- id (Primary Key): A unique identifier.
- user_id (Foreign Key): References the id in the User Table.
- post_id (Foreign Key): References the id in the Post Table.
- content: The content of the comment.
- created_at: Timestamp indicating when the comment was created.

## Vote Table:

- id (Primary Key): A unique identifier.
- user_id (Foreign Key): References the id in the User Table.
- entity_id (Foreign Key): References the id of the entity (post or comment).
- entity_type: Indicates the type of entity being voted on (e.g., 'post' or 'comment').
- vote_type: Indicates whether the vote is an 1 or -1.
- created_at: Timestamp indicating when the vote was cast.

## Subreddit Table:

- id (Primary Key): A unique identifier.
- name: The name of the subreddit.
- description: Description of the subreddit.
- created_at: Timestamp indicating when the subreddit was created.

## Subscription Table:

- id (Primary Key): A unique identifier.
- user_id (Foreign Key): References the id in the User Table
- subreddit_id(Foreign Key):References the id in the subreddit.
- created_at: Timestamp indicating when the subscription was created.
